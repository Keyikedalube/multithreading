/*
 * Program to implement deadlock using multithreading POSIX libraries
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define MAX 2

FILE* deadlock_file[MAX];
pthread_mutex_t deadlock_file0;
pthread_mutex_t deadlock_file1;

/*
 * Will be called by the thread_function if deadlock is detected
 */
void lock_error(int thread_number, int number)
{
	printf("\n\t");
	printf("thread[%d] failed to lock deadlock_file%d\n", thread_number, number);
	printf("\t");
	printf("DEADLOCK DETECTED! EXITING THE PROGRAM...\n\n");
	exit(-1);
}

void* thread_function(void* argument)
{
	// casting void type argument to int data type
	int number = *((int*) argument);
	const int thread_number = number;
	// time table = table * count
	int table;
	int count;

	switch (number) {
	case 0:
		/*
		 * thread[0] will do time table:
		 *	from 1 to 5 for deadlock_file[0]
		 *	and 16 to 20 for deadlock_file[1]
		 */
		 
		 // thread[0] locks deadlock_file0
		if (pthread_mutex_trylock(&deadlock_file0) == 0) {
			printf("thread[%d]:\tWriting to deadlock_file%d\n", thread_number, number);

			fprintf(deadlock_file[number], "thread[%d] output\n\n", thread_number);
			for (table = 1; table <= 5; table++) {
				for (count = 1; count <= 10; count++) {
					fprintf(deadlock_file[number], "%d x %d = %d\n", table, count, table*count);
				}
				fprintf(deadlock_file[number], "\n");
			}
			pthread_mutex_unlock(&deadlock_file0);
		} else
			lock_error(thread_number, number);
		
		// thread[0] locks deadlock_file1
		// since number = 0, incrementing it to match deadlock_file[1]
		number++;
		if (pthread_mutex_trylock(&deadlock_file1) == 0) {
			fprintf(deadlock_file[number], "thread[%d] output\n\n", thread_number);
			for (table = 16; table <= 20; table++) {
				for (count = 1; count <= 10; count++) {
					fprintf(deadlock_file[number], "%d x %d = %d\n", table, count, table*count);
				}
				fprintf(deadlock_file[number], "\n");
			}
			pthread_mutex_unlock(&deadlock_file1);
		} else
			lock_error(thread_number, number);
		break;
	case 1:
		/*
		 * thread[1] will do time table:
		 *	from 11 to 15 for deadlock_file[1]
		 *	and 6 to 10 for deadlock_file[0]
		 */
		 
		 // thread[1] locks deadlock_file1
		if (pthread_mutex_trylock(&deadlock_file1) == 0) {
			printf("thread[%d]:\tWriting to deadlock_file%d\n", thread_number, number);
	
			fprintf(deadlock_file[number], "thread[%d] output\n\n", thread_number);
			for (table = 11; table <= 15; table++) {
				for (count = 1; count <= 10; count++) {
					fprintf(deadlock_file[number], "%d x %d = %d\n", table, count, table*count);
				}
				fprintf(deadlock_file[number], "\n");
			}
			pthread_mutex_unlock(&deadlock_file1);
		} else
			lock_error(thread_number, number);
		
		// thread[1] locks deadlock_file0
		// since number = 1, decrementing it to match deadlock_file[0]
		number--;
		if (pthread_mutex_trylock(&deadlock_file0) == 0) {
			fprintf(deadlock_file[number], "thread[%d] output\n\n", thread_number);
			for (table = 6; table <= 10; table++) {
				for (count = 1; count <= 10; count++) {
					fprintf(deadlock_file[number], "%d x %d = %d\n", table, count, table*count);
				}
				fprintf(deadlock_file[number], "\n");
			}
			pthread_mutex_unlock(&deadlock_file0);
		} else
			lock_error(thread_number, number);
	}

	fclose(deadlock_file[number]);

	return NULL;
}

int main()
{
	pthread_t thread[MAX];
	pthread_mutex_init(&deadlock_file0, NULL);
	pthread_mutex_init(&deadlock_file1, NULL);

	int argument[MAX];
	int iterator;

	deadlock_file[0] = fopen("deadlock_file0.txt", "w");	// for thread[0]
	deadlock_file[1] = fopen("deadlock_file1.txt", "w");	// for thread[1]

	// create the two threads
	for (iterator = 0; iterator < MAX; iterator += 1) {
		argument[iterator] = iterator;
		printf("     main:\tCreating thread[%d]\n", iterator);
		pthread_create(&thread[iterator], NULL, thread_function, &argument[iterator]);
		// usleep(1);
	}

	// execute the two threads
	for (iterator = 0; iterator < MAX; iterator += 1) {
		printf("     main:\tExecuting thread[%d]\n", iterator);
		pthread_join(thread[iterator], NULL);
		// usleep(1);
	}
	
	printf("main:\tthreads execution over\n");

	// destroy all the locks
	pthread_mutex_destroy(&deadlock_file0);
	pthread_mutex_destroy(&deadlock_file1);

	return(0);
}
