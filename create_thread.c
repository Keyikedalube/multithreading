#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void* thread_function(void* arg)
{
	char* str;
	int i = 0;

	str = (char*) arg;

	while (i < 10) {
		// suspend execution to switch to main()
		usleep(1);
		printf("%s\n", str);
		++i;
	}

	return NULL;
}

int main()
{
	pthread_t pth;
	int i = 0;

	pthread_create(&pth, NULL, thread_function, "World!");

	while (i < 5) {
		// suspend execution to switch to thread_function()
		usleep(1);
		printf("Hello ");
		++i;
	}

	printf("main waiting for thread to terminate...\n");
	pthread_join(pth, NULL);

	return(0);
}
